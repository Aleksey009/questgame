﻿using UnityEngine;
using System.Collections;


public enum inventoryObjectsIds
{
	UNKNOWN,
	SPIDER_WEB, // паутина
	BROKEN_BONE, // сломанная кость
	DRY_BREAD, // старый хлеб
	FREE_RAT, // свободная крыса
	CAPTURED_RAT, // пойманная крыса
}

