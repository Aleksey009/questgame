﻿using UnityEngine;
using System.Collections;

public class applicationCore : ScriptableObject {

	private static applicationCore mInstance = null;

	public applicationCore getInstance() 
	{
		if (mInstance == null) {
			mInstance = new applicationCore ();
		}
		return mInstance;
	}
	
	public string mSceneName;
	public float mPlayingTime;

	private float mStartTime;
	
	public applicationCore()
	{
		LoadData ();
	}

	public void LoadData()
	{
		mSceneName = PlayerPrefs.GetString ("scene_name");
		mPlayingTime = PlayerPrefs.GetFloat ("play_time");

		mStartTime = Time.time;
	}

	public void SaveData()
	{
		PlayerPrefs.SetString("scene_name", mSceneName);
		PlayerPrefs.SetFloat("play_time", mPlayingTime + (Time.time - mStartTime));
		PlayerPrefs.Save ();
	}
}
