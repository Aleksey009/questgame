﻿using UnityEngine;
using System.Collections;

public class clickableObject : MonoBehaviour {

	public playerController vPlayer;
	public string [] vTextToShow;
	public bool vRandomText;
	public float vShowTime;

	private int vNextTextToShow;

	// Use this for initialization
	void Start () {
		vNextTextToShow = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Fire1")) {
			Vector3 moveToward = Camera.main.ScreenToWorldPoint( Input.mousePosition );
			moveToward.z = 0;
			
			Vector2 position;
			position.x = moveToward.x;
			position.y = moveToward.y;
			
			if(this.collider2D.OverlapPoint(position)) {
				string text;
				if(vRandomText) {
					text = vTextToShow[Random.Range(0, vTextToShow.Length)];
				}
				else {
					text = vTextToShow[vNextTextToShow];
					vNextTextToShow++;
					if(vNextTextToShow >= vTextToShow.Length) vNextTextToShow = 0;
				}
				vPlayer.showText(this, vShowTime, text);
			}
			else {
				vPlayer.hideText(this);
			}
		}
	}
}
