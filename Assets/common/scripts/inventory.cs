﻿using UnityEngine;
using System.Collections;

public class inventory : MonoBehaviour {

	public bool vOpened;
	public Collider2D vFloor;
	public Collider2D vOpenButton;
	public Collider2D [] vSlots;
	public Vector3 vClosePosition;
	public inventoryObjectTrigger[] vTriggers;
	public inventoryObject[] vAllowedObjects;

	private inventoryObject vDraggedItem;
	private Vector3 vDraggedItemPosition;
	private int vDraggedItemSortingOrder;
	private string vDraggedItemSortingLayer;
	private Vector3 vOpenPosition;

	void Start () {
		vOpened = false;
		vOpenPosition = this.transform.position;

		UpdatePos ();
	}
	
	void Update () {
		if (Input.GetButtonDown ("Fire1")) {
			Vector3 moveToward = Camera.main.ScreenToWorldPoint( Input.mousePosition );
			moveToward.z = 0;
			
			Vector2 position;
			position.x = moveToward.x;
			position.y = moveToward.y;
			
			if(vOpenButton.OverlapPoint(position)) {
				vOpened = !vOpened;
				UpdatePos();
			}
		}
	}

	void UpdatePos() {
		if(vOpened) {
			transform.position = vOpenPosition;
		}
		else {
			transform.position = vClosePosition;
		}
	}

	public bool startDragItem(inventoryObject item) {
		if(vDraggedItem != null) return false;

		if(vAllowedObjects != null && vAllowedObjects.Length > 0) {
			bool allowed = false;

			for(int i = 0;i < vAllowedObjects.Length;i++) {
				if(vAllowedObjects[i] == item) allowed = true;
			}

			if(!allowed) return false;
		}

		vDraggedItem = item;

		vDraggedItemPosition = vDraggedItem.transform.position;
		vDraggedItemSortingOrder = vDraggedItem.renderer.sortingOrder;
		vDraggedItemSortingLayer = vDraggedItem.renderer.sortingLayerName;

		vDraggedItem.renderer.sortingOrder = this.renderer.sortingOrder + 1;
		vDraggedItem.renderer.sortingLayerName = this.renderer.sortingLayerName;

		return true;
	}

	public void stopDragItem(inventoryObject item) {
		vDraggedItem = null;

		// пробуем сунуть в ячейку инвентаря

		int slot = -1;
		for(int i = 0;i < vSlots.Length;i++) {
			if(vSlots[i].bounds.Contains(item.transform.position)) {
				slot = i;
				break;
			}
		}
		if(slot == -1) {
			// проверяем активность на триггерах
			for(int i = 0;i < vTriggers.Length;i++) {
				vTriggers[i].onInventoryObjectStopDragging(item);
			}

			if(!item.trySetDefaultPosition()) {
				// не в ячейке инвентаря, пробуем кинуть на пол
				if(vFloor.collider2D.OverlapPoint(item.transform.position)) {
					item.transform.parent = null;
					item.renderer.sortingLayerName = vFloor.gameObject.renderer.sortingLayerName;
					item.renderer.sortingOrder = vFloor.gameObject.renderer.sortingOrder + 1;
				}
				else {
					item.transform.position = vDraggedItemPosition;
					item.renderer.sortingLayerName = vDraggedItemSortingLayer;
					item.renderer.sortingOrder = vDraggedItemSortingOrder;
				}
			}
		}
		else {
			// попали в ячейку
			item.transform.parent = this.transform;
			item.transform.position = vSlots[slot].transform.position;
		}
	}

	public inventoryObject getDraggedObject() {
		return vDraggedItem;
	}
}
