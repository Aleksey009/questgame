﻿using UnityEngine;
using System.Collections;

public class inventoryCombinedObject : MonoBehaviour {

	public inventoryObject [] vCombinedObjects;

	private bool vCombined;

	// Use this for initialization
	void Start () {
		vCombined = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(vCombined) return;

		Vector3 pos = vCombinedObjects [0].transform.position;

		int closeCount = 0;

		for(int i = 0;i < vCombinedObjects.Length;i++) {
			Vector3 dist = pos - vCombinedObjects[i].transform.position;

			if(dist.magnitude < 0.5) {
				closeCount++;
			}
		}

		if(closeCount == vCombinedObjects.Length) {
			//inventory inv = vCombinedObjects[0].vInventory;
			//inventoryObject draggedObject = inv.getDraggedObject ();

			//inv.stopDragItem(draggedObject);

			// выключаем старые объекты
			for(int i = 0;i < vCombinedObjects.Length;i++) {
				vCombinedObjects[i].gameObject.SetActive(false);
			}

			this.transform.position = pos;
			vCombined = true;
		}
	}


}
