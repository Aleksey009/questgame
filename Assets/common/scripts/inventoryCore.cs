﻿using UnityEngine;
using System.Collections;

public class inventoryCore : ScriptableObject {

	private static int MAX_ITEMS_COUNT = 8;
	private static inventoryCore mInstance = null;

	public enum eErrorCodes
	{
		NO_ERROR,
		INVALID_SLOT,
		SLOT_ALREADY_HAVE_ITEM,
	};
	public static int INVALID_SLOT_ID = -1;
	
	public inventoryCore getInstance() 
	{
		if (mInstance == null) {
			mInstance = new inventoryCore ();
		}
		return mInstance;
	}
	
	private ArrayList mListeners;
	private inventoryObject [] mItems;

	public inventoryCore()
	{
		mListeners = new ArrayList ();

		mItems = new inventoryObject[MAX_ITEMS_COUNT];
		for(int i = 0;i < mItems.Length;i++) {
			mItems[i] = null;
		}
	}

	public void addListener(inventoryCoreListener listener)
	{
		mListeners.Add (listener);
	}

	public void removeListener(inventoryCoreListener listener)
	{
		mListeners.Remove (listener);
	}

	public eErrorCodes setItem(inventoryObject item, int slotid)
	{
		/*
		 * Могут быть случаи: 
		 * ставим в слот этот же объект (начали тянуть и вернули обратно)
		 * ставим объект не из инвентаря в пустой слот
		 * ставим объект не из инвентаря в занятый слот
		 * ставим объект из одного слота в другой занятый
		 * ставим объект из одного слота в другой свободный
		 */

		if(slotid < 0 || slotid >= MAX_ITEMS_COUNT) return eErrorCodes.INVALID_SLOT;

		int oldslotid = getItemSlot (item);

		if(oldslotid == slotid) {
			return eErrorCodes.NO_ERROR;
		}
		else if(oldslotid == INVALID_SLOT_ID) {
			if(mItems[slotid] == null) {
				mItems[slotid] = item;

				foreach(inventoryCoreListener listener in mListeners) {
					listener.onItemTaken(item, slotid);
				}
			}
			else {
				foreach(inventoryCoreListener listener in mListeners) {
					listener.onItemDroped(mItems[slotid], slotid);
				}

				mItems[slotid] = item;

				foreach(inventoryCoreListener listener in mListeners) {
					listener.onItemTaken(item, slotid);
				}
			}
		}
		else {
			mItems[oldslotid] = mItems[slotid];
			mItems[slotid] = item;

			foreach(inventoryCoreListener listener in mListeners) {
				if(mItems[oldslotid] != null) listener.onItemMoved(mItems[oldslotid], slotid, oldslotid);
				listener.onItemMoved(item, oldslotid, slotid);
			}
		}

		return eErrorCodes.NO_ERROR;
	}

	public inventoryObject getItem(int slotid)
	{
		if(slotid < 0 || slotid >= MAX_ITEMS_COUNT) return null;

		return mItems [slotid];
	}

	public int getItemSlot(inventoryObject item) 
	{
		for(int i = 0;i < mItems.Length;i++) {
			if(mItems[i] == item) return i;
		}
		return INVALID_SLOT_ID;
	}

	public eErrorCodes loadData()
	{
		for(int i = 0;i < mItems.Length;i++) {
			int type = PlayerPrefs.GetInt("inventory_slot_" + i);

			GameObject go = Instantiate(Resources.Load("MyPrefab")) as GameObject;
		}

		return eErrorCodes.NO_ERROR;
	}

	public eErrorCodes saveData()
	{
		return eErrorCodes.NO_ERROR;
	}
}
