﻿using UnityEngine;
using System.Collections;

public interface inventoryCoreListener {

	void onItemTaken(inventoryObject item, int slotid);
	void onItemDroped(inventoryObject item, int slotid);
	void onItemMoved(inventoryObject item, int fromslotid, int toslotid);
}
