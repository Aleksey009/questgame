﻿using UnityEngine;
using System.Collections;

public class inventoryObject : MonoBehaviour {

	public inventoryObjectsIds vId;
	public bool vCanSetBack;
	public bool vNotUsable;

	private bool isDrag;
	private Vector3 lastPos;
	private Vector3 defaultPosition;
	private Transform defaultParent;
	private int defaultSortingOrder;
	private string defaultSortingLayer;

	// Use this for initialization
	void Start () {
		isDrag = false;

		defaultPosition = this.transform.position;
		defaultParent = this.transform.parent;
		defaultSortingOrder = this.renderer.sortingOrder;
		defaultSortingLayer = this.renderer.sortingLayerName;
	}

	/*
	// Update is called once per frame
	void Update () {
		if(vNotUsable) return;

		Vector3 point = Camera.main.ScreenToWorldPoint( Input.mousePosition );
		point.z = 0;

		if (Input.GetButtonDown ("Fire1")) {
			if(this.renderer.bounds.Contains(point)) {
				if(vInventory.startDragItem(this)) {
					isDrag = true;
					lastPos = point;
				}
			}
		}
		if(Input.GetButtonUp("Fire1")) {
			if(isDrag) {
				isDrag = false;
				vInventory.stopDragItem(this);
			}
		}

		if (isDrag) {
			Vector3 curpos = transform.position;
			curpos.x += (point.x - lastPos.x);
			curpos.y += (point.y - lastPos.y);

			this.transform.position = curpos;
			lastPos = point;
		}
	}
	*/

	public void setDefaultSortingSettings()
	{
		this.renderer.sortingLayerName = defaultSortingLayer;
		this.renderer.sortingOrder = defaultSortingOrder;
	}

	public bool trySetDefaultPosition() {
		if(!vCanSetBack) return false;

		Vector3 dist = defaultPosition - this.transform.position;

		if(dist.magnitude < 0.5f) {
			this.transform.position = defaultPosition;
			this.transform.parent = defaultParent;
			setDefaultSortingSettings();
			return true;
		}
		else {
			return false;
		}
	}
}
