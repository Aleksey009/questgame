﻿using UnityEngine;
using System.Collections;

public class inventoryObjectTrigger : MonoBehaviour {

	public inventoryObject [] vTriggerObjects;
	public GameObject vAction;

	private bool [] vTriggered;

	// Use this for initialization
	void Start () {
		vTriggered = new bool[vTriggerObjects.Length];
		for(int i = 0;i < vTriggered.Length;i++) {
			vTriggered[i] = false;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public bool onInventoryObjectStopDragging(inventoryObject item) {
		if(!this.collider2D.OverlapPoint(item.transform.position)) {
			return false;
		}

		bool founded = false;
		int triggeredCount = 0;
		for(int i = 0;i < vTriggerObjects.Length;i++) {
			if(vTriggerObjects[i] == item) {
				vTriggered[i] = true;
				founded = true;
			}

			if(vTriggered[i]) triggeredCount++;
		}

		if(triggeredCount == vTriggerObjects.Length) {
			vAction.SendMessage("onTrigger", this);
			for(int i = 0;i < vTriggered.Length;i++) {
				vTriggered[i] = false;
			}
		}

		return founded;
	}
}
