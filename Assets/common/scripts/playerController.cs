﻿using UnityEngine;
using System.Collections;



public class playerController : MonoBehaviour {
	public enum ePlayerState
	{
		PLAYER_STATE_UNKNOWN,
		PLAYER_STATE_FREEZED,
		PLAYER_STATE_IDLE,
		PLAYER_STATE_MOVING
	};

	public PolygonCollider2D vFloor;
	public float vMovingSpeed;
	public ePlayerState vState;
	public GUIText vGUIText;
	public float vTextHeight;
	
	private Vector3 vMoveTo;
	private ePlayerState vCurrentState;
	private float vTextEndTime;
	private bool vIsTextShowed;
	private MonoBehaviour vTextCaller;

	void Start () {
		vGUIText.text = "";
		vIsTextShowed = false;
		vTextCaller = null;
		vCurrentState = ePlayerState.PLAYER_STATE_UNKNOWN;

		tryChangeState (vState);

		vMoveTo = this.transform.position;
	}

	void Update () {
		// attached text
		Vector3 textpos = this.transform.position;
		textpos.y += vTextHeight;

		vGUIText.transform.position = Camera.main.WorldToViewportPoint(textpos);
		
		if(vIsTextShowed && (vTextEndTime != 0) && (Time.time > vTextEndTime)) {
			hideText(vTextCaller);
		}

		// control
		switch(vCurrentState) {
		case ePlayerState.PLAYER_STATE_IDLE:
		case ePlayerState.PLAYER_STATE_MOVING:
		{
			if (Input.GetButtonDown ("Fire1")) {
				Vector3 moveToward = Camera.main.ScreenToWorldPoint( Input.mousePosition );
				moveToward.z = 0;
				
				Vector2 position;
				position.x = moveToward.x;
				position.y = moveToward.y;
				
				if(vFloor.OverlapPoint(position)) {
					if(tryChangeState(ePlayerState.PLAYER_STATE_MOVING)) {
						vMoveTo = moveToward;
					}
				}
			}
			break;
		}
		}

		// update
		switch(vCurrentState) {
		case ePlayerState.PLAYER_STATE_IDLE:
		{

			break;
		}
		case ePlayerState.PLAYER_STATE_MOVING:
		{
			Vector3 curpos = this.transform.position;

			if ((Mathf.Abs(curpos.x - vMoveTo.x) > 0.05) ||
			    (Mathf.Abs(curpos.y - vMoveTo.y) > 0.05)) {
				Vector3 newpos = Vector3.MoveTowards(curpos, vMoveTo, vMovingSpeed * Time.deltaTime);

				if(vFloor.OverlapPoint(newpos)) {
					this.transform.position = newpos;
						
					Vector3 scale = this.transform.localScale;
					if(curpos.x - vMoveTo.x > 0) {
						scale.x = -Mathf.Abs(scale.x);
					}
					else {
						scale.x = Mathf.Abs(scale.x);
					}
					this.transform.localScale = scale;
				}
				else {
					tryChangeState(ePlayerState.PLAYER_STATE_IDLE);
				}
			}
			else {
				tryChangeState(ePlayerState.PLAYER_STATE_IDLE);
			}
			break;
		}
		}
	}

	public bool tryChangeState(ePlayerState state) {
		if (vCurrentState == state) {
			return true;
		}

		if(state == ePlayerState.PLAYER_STATE_IDLE) {
			switch(vCurrentState) {
				case ePlayerState.PLAYER_STATE_UNKNOWN:
				case ePlayerState.PLAYER_STATE_MOVING:
				{
					vCurrentState = ePlayerState.PLAYER_STATE_IDLE;
					this.GetComponent<Animator>().SetBool("idle", true);
					return true;
				}
			}
		}
		if(state == ePlayerState.PLAYER_STATE_MOVING) {
			switch(vCurrentState) {
				case ePlayerState.PLAYER_STATE_IDLE:
				{
					vCurrentState = ePlayerState.PLAYER_STATE_MOVING;
					this.GetComponent<Animator>().SetBool("idle", false);
					return true;
				}
			}
		}

		return false;
	}

	public void showText(MonoBehaviour caller, float time, string text)
	{
		vGUIText.text = text;
		if(time != 0) vTextEndTime = Time.time + time;
		else vTextEndTime = 0;
		vIsTextShowed = true;
		vTextCaller = caller;
	}
	
	public void hideText(MonoBehaviour caller)
	{
		if(vTextCaller != caller) return;
		
		vGUIText.text = "";
		vIsTextShowed = false;
	}
}
