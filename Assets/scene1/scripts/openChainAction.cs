﻿using UnityEngine;
using System.Collections;

public class openChainAction : MonoBehaviour {

	public inventory vInventory;
	public SpriteRenderer vChain;
	public playerController vPlayer;
	public Sprite vOpenedSprite;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void onTrigger(inventoryObjectTrigger trigger) {
		if(vPlayer.tryChangeState (playerController.ePlayerState.PLAYER_STATE_IDLE)) {
			vChain.sprite = vOpenedSprite;
			vChain.renderer.sortingOrder = 0;
			vPlayer.showText(this, 5, "Я свободен!");
			vInventory.vAllowedObjects = null;

			for(int i = 0;i < trigger.vTriggerObjects.Length;i++) {
				trigger.vTriggerObjects[i].gameObject.SetActive(false);
			}
		}
	}
}
